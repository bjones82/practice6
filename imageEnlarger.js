var imgArray = document.getElementsByTagName("img");
for(var i = 0; i < imgArray.length; i++ ) {
    imgArray[i].addEventListener("mouseover",function(e){enlarger(e.target)});
    imgArray[i].addEventListener("mouseout",function(e){remover(e.target)});
}

function enlarger(picture) {
    console.log("Enlarging...");
    var newSpan = document.createElement("span");
    var largerPic = document.createElement("img");
    largerPic.setAttribute("src",picture.src);
    console.log(2 * picture.height);
    largerPic.height = 2 * picture.height;
    largerPic.width = 2 * picture.width;
    newSpan.appendChild(largerPic);
    picture.parentNode.appendChild(newSpan);
}

function remover(picture) {
    console.log("Removing...");
    picture.parentNode.removeChild(picture.nextSibling);
}